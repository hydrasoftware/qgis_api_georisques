# -*- coding: utf-8 -*-

from qgis.PyQt.QtCore import QCoreApplication, QVariant
from qgis.core import (QgsProcessing,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterVectorLayer,
                       QgsProcessingParameterEnum,
                       QgsProject,
                       QgsCoordinateReferenceSystem,
                       QgsCoordinateTransformContext,
                       QgsCoordinateTransform,
                       QgsMessageLog,
                       QgsDistanceArea,
                       Qgis,
                       QgsField,
                       QgsVectorLayer,
                       QgsFeature,
                       QgsGeometry,
                       QgsPointXY)
import requests
import datetime
import math
import json

# d10000 = 10000.0
requestDistance = 10000
url_api_geo = "https://geo.api.gouv.fr/communes"
list_options = ['AZI',
                'CATNAT',
                'Cavites',
                'DICRIM',
                'Installations classées',
                'MVT',
                'PAPI',
                'PCS',
                'PPR',
                'Radon',
                'Risques',
                'SIS',
                'TIM',
                'TRI',
                'Zonage sismique']

processOptionsList = ["Limiter les résultats au recoupement du périmètre",
                      "Conserver les résultats autour de l'étendue du périmètre"]

class AlgoAPIGeorisques(QgsProcessingAlgorithm):


    OUTPUT = 'OUTPUT'
    INPUT = 'INPUT'

    def initAlgorithm(self, config):

        repProjet = QgsProject.instance().readPath("./")
        
        self.addParameter(QgsProcessingParameterVectorLayer('layerprojectperimeter', "Couche polygone d'emprise de projet", types=[QgsProcessing.TypeVectorPolygon], defaultValue=None))
        self.addParameter(QgsProcessingParameterEnum('options', 'Options', options=list_options, allowMultiple=True, defaultValue=[0]))
        self.addParameter(QgsProcessingParameterEnum('processoption', 'Traitement du résultat', options=processOptionsList, allowMultiple=False, defaultValue=[0]))

    def processAlgorithm(self, parameters, context, feedback):
        
        date1 = datetime.datetime.now()
        
        layerprojectperimeter = Assign_Layer_Vector(parameters['layerprojectperimeter'])
        processOption = parameters['processoption']
        
        srcProjSRID = layerprojectperimeter.crs().postgisSrid()
        
        #Create transformation instances
        srcProjRef = QgsCoordinateReferenceSystem(layerprojectperimeter.crs().authid())       
        
        proj2154 = QgsCoordinateReferenceSystem(2154)
        proj4326 = QgsCoordinateReferenceSystem(4326)
        
        tform = QgsCoordinateTransform(srcProjRef, proj4326, QgsProject.instance())
        tform_2154 = QgsCoordinateTransform(proj2154, srcProjRef, QgsProject.instance())
        tform_to_2154 = QgsCoordinateTransform(srcProjRef, proj2154, QgsProject.instance())
        
        #First get translation distance of current proj corresponding to 10000 meters
        
        pt1 = QgsPointXY(620000.0,6700000.0)
        pt2 = QgsPointXY(630000.0,6700000.0)
        
        pt1T = tform_2154.transform(pt1)
        pt2T = tform_2154.transform(pt2)
        
        #Update d10000 value
        
        d10000 = QgsGeometry().fromPointXY(pt1T).distance(QgsGeometry().fromPointXY(pt2T))
        
        #start process
        #get project extent
        
        i = 0

        if layerprojectperimeter.selectedFeatureCount() == 0 :
        
            return {self.OUTPUT: "Processus non effectué - une sélection active doit être présente sur le polygone d'emprise pour pouvoir effectuer le travail"}
        
        for extentfeature in layerprojectperimeter.getSelectedFeatures() :
            
            if i >= 1 : break

            #polygon entry
            projectPerimeterGeom = extentfeature.geometry()
            
            i += 1
            
        if list_options.index("AZI") in parameters['options'] :
            
            request_AZI(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000)
            
        if list_options.index("CATNAT") in parameters['options'] :
            
            request_CATNAT(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000)
            
        if list_options.index("Cavites") in parameters['options'] :
            
            request_Cavites(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000)
            
        if list_options.index("DICRIM") in parameters['options'] :
            
            request_DICRIM(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000)
            
        if list_options.index("Installations classées") in parameters['options'] :
            
            request_Installations_Classees(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000)
            
        if list_options.index("MVT") in parameters['options'] :
            
            request_MVT(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000)
            
        if list_options.index('PAPI') in parameters['options'] :
            
            request_PAPI(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000)
            
        if list_options.index('PCS') in parameters['options'] :
            
            request_PCS(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000)

        if list_options.index("PPR") in parameters['options'] :
            
            request_PPR(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000)
            
        if list_options.index("Radon") in parameters['options'] :
            
            request_Radon(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000)
            
        if list_options.index("Risques") in parameters['options'] :
            
            request_Risques(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000)
            
        if list_options.index("SIS") in parameters['options'] :
            
            request_SIS(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000)
            
        if list_options.index("TIM") in parameters['options'] :
            
            request_TIM(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000)
            
        if list_options.index("TRI") in parameters['options'] :
            
            request_TRI(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000)
            
        if list_options.index("Zonage sismique") in parameters['options'] :
            
            request_Zonage_Sismique(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000)

        return {self.OUTPUT: 'Processus terminé de manière nominale'}

    def name(self):

        return 'Interrogation API Georisques - Périmètres'

    def displayName(self):

        return self.name()

    def group(self):

        return self.groupId()

    def groupId(self):

        return "API Georisques"

    def createInstance(self):
        
        return AlgoAPIGeorisques()
    
    def shortHelpString(self):
        """
        Returns a localised short help string for the algorithm.
        """
        return ("L'outil permet d'interroger l'API Georisques et de convertir les résultats en entités géographiques lisibles sous SIG \n"  + \
        "L'ensemble des résultats retournés par l'API sont intégrés en tant que données attributaires\n" + \
        "<h3>Sélection des paramètres</h3>\n" + \
        "Les paramètres sont les suivants : \n" + \
        "- couche de type Polygone, existante ou créée en mamoire pour une utilisation had'oc, représentant le périmètre géographique sur lequel l'API Géorisque sera interrogée\n " + \
        "- Choix des informations à rapatrier\n " + \
        "- Choix de l'option soit de ne conserver que les résultats recoupant le périmètre, soit de conserver également les résultats aux alentours du périmètre\n " + \
        "<h3>Exécution de l'algorithme</h3> \n" + \
        "<b>Une sélection active d'un périmètre est obligatoire, sinon le processus ne retournera aucun résultat<b>\n" + \
        "<b>si une sélection multiple est appliquée, les résultats ne sont fondées que sur le premier périmètre<b>\n" + \
        "A partir de la géométrie du périmètre de référence, l'étendue du polygone est calculée et l'espace est sous-découpé en plusieurs points d'interrogation correspondant à la distance maximale autorisée par l'API Géorisques \n" + \
        "L'ensemble de ces points d'interrogations retournent des résultats qui sont assemblés dans une couche temporaire résultat avec le type d'information comme intitulé\n"
        )
    
def Assign_Layer_Vector(stringLayer):

    layerExt = None

    layers = QgsProject.instance().mapLayers().values()

    for layer in layers :

        if layer.id() == stringLayer :

            layerExt = layer

    if not layerExt and os.path.exists(stringLayer) :

        layerExt = QgsVectorLayer(stringLayer.replace("\\","/"), '', 'ogr')

    if not layerExt and len(stringLayer.split('|layername=')) > 1 :

        layerExt = QgsVectorLayer(stringLayer, '', 'ogr')

    return layerExt

def getListsXY(xMin,xMax,yMin,yMax,d10000):
        
    listX = [xMin]

    xTest = xMin + d10000
    
    pt1 = QgsGeometry.fromPointXY(QgsPointXY(xMin, 1.0))
    pt2 = QgsGeometry(pt1)
    pt2.translate(d10000, 0.0)
    ptMax = QgsGeometry.fromPointXY(QgsPointXY(xMax, 1.0))

    distTest = pt2.distance(ptMax)
    
    while distTest >= 2.0*d10000 :
    
        listX.append(pt2.asPoint().x())
        pt2.translate(d10000, 0.0)
        distTest = pt2.distance(ptMax)
    
    listX.append(xMax)

    
    listY = [yMin]
    
    yTest = yMin + d10000
    
    pt1 = QgsGeometry.fromPointXY(QgsPointXY(1.0, yMin))
    pt2 = QgsGeometry(pt1)
    pt1.translate(0.0, d10000)
    ptMax = QgsGeometry.fromPointXY(QgsPointXY(1.0, yMax))

    distTest = pt2.distance(ptMax)
    
    while distTest >= 2.0*d10000 :
    
        listY.append(pt2.asPoint().y())
        pt2.translate(0.0, d10000)
        distTest = pt2.distance(ptMax)
    
    listY.append(yMax)
    
    return listX, listY

def getMultiPolygonContourFromINSEE(code_insee_req,tform):

    QgsMessageLog.logMessage("code_insee_req".format(code_insee_req=code_insee_req),tag="Data Processing", level=Qgis.Info)
    
    if code_insee_req[:2] == '75' :
        
        code_insee_req = '75056'
    
    params_api_geo = {
                        'code' : code_insee_req,
                        'format' : 'geojson',
                        'geometry' : 'contour'
                    }
            
    formatted_url_api_geo = url_api_geo + '?' + '&'.join(["{}={}".format(k,v) for k,v in params_api_geo.items()])
    
    response_api_geo = requests.get(url=formatted_url_api_geo)

    #Jsonify API response        
    data_api_geo = response_api_geo.json()
    
    # QgsMessageLog.logMessage("data_api_geo " + str(data_api_geo),tag="Data Processing", level=Qgis.Info)
    
    cityFeature = data_api_geo["features"][0]

    newGeom = QgsGeometry()
    
    parts = cityFeature["geometry"]["coordinates"]
    
    # QgsMessageLog.logMessage("code_insee_req " + str(code_insee_req),tag="Data Processing", level=Qgis.Info)
    # QgsMessageLog.logMessage("len(parts) " + str(len(parts)),tag="Data Processing", level=Qgis.Info)
    # QgsMessageLog.logMessage("parts " + str(parts),tag="Data Processing", level=Qgis.Info)

    listParts = []
    
    if len(parts) == 1 :
    
        # for i_part in range(len(parts)) :
            
        listCoordinates = parts[0]
        
        # QgsMessageLog.logMessage("listCoordinates " + str(listCoordinates),tag="Data Processing", level=Qgis.Info)
        
        listNewPart = []
        
        for coords in listCoordinates :
            
            long = coords[0]
            lat = coords[1]
            
            ptRev = QgsGeometry.fromPointXY(QgsPointXY(long, lat))
            ptRevT = tform.transform(ptRev.asPoint(),QgsCoordinateTransform.ReverseTransform)
            
            listNewPart.append(QgsPointXY(ptRevT.x(),ptRevT.y()))
            
        listParts.append(listNewPart)
    
    else :
    
        for i_part in range(len(parts)) :
    
            # QgsMessageLog.logMessage("parts[i_part] " + str(parts[i_part]),tag="Data Processing", level=Qgis.Info)
                
            listCoordinates = parts[i_part][0]
            
            # QgsMessageLog.logMessage("listCoordinates " + str(listCoordinates),tag="Data Processing", level=Qgis.Info)
            
            listNewPart = []
            
            for coords in listCoordinates :
                
                long = coords[0]
                lat = coords[1]
                
                ptRev = QgsGeometry.fromPointXY(QgsPointXY(long, lat))
                ptRevT = tform.transform(ptRev.asPoint(),QgsCoordinateTransform.ReverseTransform)
                
                listNewPart.append(QgsPointXY(ptRevT.x(),ptRevT.y()))
                
            listParts.append(listNewPart)

    newGeom = QgsGeometry.fromMultiPolygonXY([listParts])
    
    return newGeom

def getINSEECodeFromLonLat(longitude,latitude):
    
    params_api_geo = {
                        'lon' : longitude,
                        'lat' : latitude
                    }
            
    formatted_url_api_geo = url_api_geo + '?' + '&'.join(["{}={}".format(k,v) for k,v in params_api_geo.items()])
  
    response_api_geo = requests.get(url=formatted_url_api_geo)
     
    data_api_geo = response_api_geo.json()
    
    cityINSEE = data_api_geo[0]["code"]
    
    return cityINSEE

def removeDuplicates(listNewFeats):
    
    if len(listNewFeats) <= 1 :
        
        return listNewFeats
    
    listFieldsNames = [f.name() for f in listNewFeats[0].fields()]
    
    iMax = 0
    
    lenListNewFeats = len(listNewFeats)
    
    for i in (range(lenListNewFeats)) :
        
        if i >= len(listNewFeats)-1 : break
    
        iFeat = listNewFeats[i]
    
        i_geometry = iFeat.geometry()
    
        for j in reversed(range(i+1,len(listNewFeats))) :
    
            jFeat = listNewFeats[j]
    
            j_geometry = jFeat.geometry()
    
            if i_geometry.equals(j_geometry) :
    
                fieldEquals = True
    
                for fieldName in listFieldsNames :
    
                    if iFeat[fieldName] != jFeat[fieldName] :
    
                        fieldEquals = False
    
                if fieldEquals :
    
                    listNewFeats.pop(j)
                    
        lenListNewFeats = len(listNewFeats)
                    
    return listNewFeats

def request_AZI(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000):
    
    # API url
        
    url = "https://www.georisques.gouv.fr/api/v1/gaspar/azi"
    
    
    #Temporary layer for resulting polygons
    layerResult = QgsVectorLayer("MultiPolygon?crs=epsg:"+str(srcProjSRID), "API Géorisques - Périmètres AZI", "memory")
    layerResultProvider = layerResult.dataProvider()
    
    layerResultProvider.addAttributes([QgsField("code_national_azi",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_azi",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("liste_libelle_risque",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_bassin_risques",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_commentaire",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_debut_programmation",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_fin_programmation",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_debut_etude",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_fin_etude",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_debut_information",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_fin_information",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_realisation",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_diffusion",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_publication_web",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("code_insee",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_commune",QVariant.String, "string", 0)])
    
    layerResult.updateFields()

    listFieldsNames = [f.name() for f in layerResult.fields()]
    
    #list for insert
    
    listNewFeats = []
    
    #Get bounding box
    
    bbox = projectPerimeterGeom.boundingBox()
            
    xMin = bbox.xMinimum()
    xMax = bbox.xMaximum()
    yMin = bbox.yMinimum()
    yMax = bbox.yMaximum()
    
    # QgsMessageLog.logMessage("{xMin} {xMax} {yMin} {yMax}".format(xMin=xMin,xMax=xMax,yMin=yMin,yMax=yMax),tag="Data Processing", level=Qgis.Info)

    #decompose extent rectangle by future request radius
    
    listX, listY = getListsXY(xMin,xMax,yMin,yMax,d10000)

    #Requests on Georisques API
    QgsMessageLog.logMessage("Requests on Georisques API - AZI",tag="Data Processing", level=Qgis.Info)
    
    for xReq in listX :
        
        for yReq in listY :
            
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {
                'rayon' : requestDistance,
                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
                
            #Jsonify API response        
            data = response.json()
            
            # QgsMessageLog.logMessage("data " + str(data),tag="Data Processing", level=Qgis.Info)
            
            # QgsMessageLog.logMessage("data {data}".format(data=str(data)),tag="Data Processing", level=Qgis.Info)
            
            if "data" in data.keys() :
                
                if not data["data"] : continue
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    if "code_national_azi" in currentResult.keys() :
                        code_national_azi = currentResult["code_national_azi"]
                    else :
                        code_national_azi = ''
                    
                    if "libelle_azi" in currentResult.keys() :
                        libelle_azi = currentResult["libelle_azi"]
                    else :
                        libelle_azi = ''
                        
                    if "liste_libelle_risque" in currentResult.keys() :
                        liste_libelle_risque = str(currentResult["liste_libelle_risque"])
                    else :
                        liste_libelle_risque = ''
                        
                    if "libelle_bassin_risques" in currentResult.keys() :
                        libelle_bassin_risques = str(currentResult["libelle_bassin_risques"])
                    else :
                        libelle_bassin_risques = ''
                        
                    if "libelle_commentaire" in currentResult.keys() :
                        libelle_commentaire = str(currentResult["libelle_commentaire"])
                    else :
                        libelle_commentaire = ''
                        
                    if "date_debut_programmation" in currentResult.keys() :
                        date_debut_programmation = str(currentResult["date_debut_programmation"])
                    else :
                        date_debut_programmation = ''
                        
                    if "date_fin_programmation" in currentResult.keys() :
                        date_fin_programmation = str(currentResult["date_fin_programmation"])
                    else :
                        date_fin_programmation = ''
                        
                    if "date_debut_etude" in currentResult.keys() :
                        date_debut_etude = str(currentResult["date_debut_etude"])
                    else :
                        date_debut_etude = ''
                        
                    if "date_fin_etude" in currentResult.keys() :
                        date_fin_etude = str(currentResult["date_fin_etude"])
                    else :
                        date_fin_etude = ''
                        
                    if "date_debut_information" in currentResult.keys() :
                        date_debut_information = str(currentResult["date_debut_information"])
                    else :
                        date_debut_information = ''
                        
                    if "date_fin_information" in currentResult.keys() :
                        date_fin_information = str(currentResult["date_fin_information"])
                    else :
                        date_fin_information = ''
                        
                    if "date_realisation" in currentResult.keys() :
                        date_realisation = str(currentResult["date_realisation"])
                    else :
                        date_realisation = ''
                        
                    if "date_diffusion" in currentResult.keys() :
                        date_diffusion = str(currentResult["date_diffusion"])
                    else :
                        date_diffusion = ''
                        
                    if "date_publication_web" in currentResult.keys() :
                        date_publication_web = str(currentResult["date_publication_web"])
                    else :
                        date_publication_web = ''
                        
                    if "code_insee" in currentResult.keys() :
                        code_insee = str(currentResult["code_insee"])
                    else :
                        code_insee = ''
                        
                    if "libelle_commune" in currentResult.keys() :
                        libelle_commune = str(currentResult["libelle_commune"])
                    else :
                        libelle_commune = ''

                    code_insee_req = code_insee.zfill(5)
                    
                    newFeat = QgsFeature()
                    newFeat.setFields(layerResult.fields())
                    
                    newGeom = getMultiPolygonContourFromINSEE(code_insee_req,tform)
                    
                    if processOption == 0 :
                        
                        if not newGeom.intersects(projectPerimeterGeom) : continue
                        
                    newFeat.setGeometry(newGeom)
                    newFeat.setAttribute("code_national_azi",code_national_azi)
                    newFeat.setAttribute("libelle_azi",libelle_azi)
                    newFeat.setAttribute("liste_libelle_risque",liste_libelle_risque)
                    newFeat.setAttribute("libelle_bassin_risques",libelle_bassin_risques)
                    newFeat.setAttribute("date_debut_programmation",date_debut_programmation)
                    newFeat.setAttribute("date_fin_programmation",date_fin_programmation)
                    newFeat.setAttribute("date_debut_etude",date_debut_etude)
                    newFeat.setAttribute("date_fin_etude",date_fin_etude)
                    newFeat.setAttribute("date_debut_information",date_debut_information)
                    newFeat.setAttribute("date_fin_information",date_fin_information)
                    newFeat.setAttribute("date_diffusion",date_diffusion)
                    newFeat.setAttribute("date_publication_web",date_publication_web)
                    newFeat.setAttribute("code_insee",code_insee)
                    newFeat.setAttribute("libelle_commune",libelle_commune)
                    
                    listNewFeats.append(newFeat)

    listNewFeats = removeDuplicates(listNewFeats)
    
    layerResult.dataProvider().addFeatures(listNewFeats)
    layerResult.commitChanges()

    #End of nodes insertion; update temp layer extents

    layerResult.updateExtents()
    
    QgsProject.instance().addMapLayer(layerResult)
    
    del url, layerResult, layerResultProvider, listFieldsNames, listNewFeats
    del bbox, xMin, xMax, yMin, yMax, listX, listY
    
    try :
        del ptReq, ptReqT, params, formatted_url, response, data, currentResult, newFeat, newGeom
    except :
        pass
    
    try :
        del code_national_azi, libelle_azi, liste_libelle_risque, libelle_bassin_risques, libelle_commentaire
        del date_debut_programmation, date_fin_programmation, date_debut_etude, date_fin_etude
        del date_debut_information, date_fin_information, date_realisation, date_diffusion
        del date_publication_web, code_insee, libelle_commune, code_insee_req
    except :
        pass
    
def request_CATNAT(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000):
    
    # API url
        
    url = "https://www.georisques.gouv.fr/api/v1/gaspar/catnat"
    
    
    #Temporary layer for resulting polygons
    layerResult = QgsVectorLayer("MultiPolygon?crs=epsg:"+str(srcProjSRID), "API Géorisques - Arrêtés CATNAT", "memory")
    layerResultProvider = layerResult.dataProvider()
    
    layerResultProvider.addAttributes([QgsField("code_insee",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("code_national_catnat",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_debut_evt",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_fin_evt",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_publication_arrete",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_publication_jo",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_commune",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_risque_jo",QVariant.String, "string", 0)])
        
    layerResult.updateFields()
    
    listFieldsNames = [f.name() for f in layerResult.fields()]
    
    #list for insert
    
    listNewFeats = []
    
    #Get bounding box
    
    bbox = projectPerimeterGeom.boundingBox()
            
    xMin = bbox.xMinimum()
    xMax = bbox.xMaximum()
    yMin = bbox.yMinimum()
    yMax = bbox.yMaximum()

    #decompose extent rectangle by future request radius
    
    listX, listY = getListsXY(xMin,xMax,yMin,yMax,d10000)

    #Requests on Georisques API
    QgsMessageLog.logMessage("Requests on Georisques API - CATNAT",tag="Data Processing", level=Qgis.Info)
    
    for xReq in listX :
        
        for yReq in listY :
            
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {
                'rayon' : requestDistance,
                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                if not data["data"] : continue
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    if "code_insee" in currentResult.keys() :
                        code_insee = currentResult["code_insee"]
                    else :
                        code_insee = ''
                        
                    code_national_catnat = currentResult["code_national_catnat"]
                    date_debut_evt = str(currentResult["date_debut_evt"])
                    date_fin_evt = str(currentResult["date_fin_evt"])
                    date_publication_arrete = str(currentResult["date_publication_arrete"])
                    date_publication_jo = str(currentResult["date_publication_jo"])
                    libelle_commune = str(currentResult["libelle_commune"])
                    libelle_risque_jo = str(currentResult["libelle_risque_jo"])
                    
                    code_insee_req = code_insee.zfill(5)

                    newFeat = QgsFeature()
                    newFeat.setFields(layerResult.fields())
                    
                    newGeom = getMultiPolygonContourFromINSEE(code_insee_req,tform)
                    
                    if processOption == 0 :
                        
                        if not newGeom.intersects(projectPerimeterGeom) : continue
                        
                    newFeat.setGeometry(newGeom)
                    newFeat.setAttribute("code_insee",code_insee)
                    newFeat.setAttribute("code_national_catnat",code_national_catnat)
                    newFeat.setAttribute("date_debut_evt",date_debut_evt)
                    newFeat.setAttribute("date_fin_evt",date_fin_evt)
                    newFeat.setAttribute("date_publication_arrete",date_publication_arrete)
                    newFeat.setAttribute("date_publication_jo",date_publication_jo)
                    newFeat.setAttribute("libelle_commune",libelle_commune)
                    newFeat.setAttribute("libelle_risque_jo",libelle_risque_jo)
                    
                    listNewFeats.append(newFeat)

    listNewFeats = removeDuplicates(listNewFeats)
    
    layerResult.dataProvider().addFeatures(listNewFeats)
    layerResult.commitChanges()

    #End of nodes insertion; update temp layer extents

    layerResult.updateExtents()
    
    QgsProject.instance().addMapLayer(layerResult)
    
    del url, layerResult, layerResultProvider, listFieldsNames, listNewFeats
    del bbox, xMin, xMax, yMin, yMax, listX, listY
    
    try :
        del ptReq, ptReqT, params, formatted_url, response, data, currentResult, newFeat, newGeom
    except :
        pass
    
    try :
        del code_insee, code_national_catnat, date_debut_evt, date_fin_evt
        del date_publication_arrete, date_publication_jo, libelle_commune, libelle_risque_jo, code_insee_req
    except :
        pass

def request_Cavites(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000):
    
    # API url
        
    url = "https://www.georisques.gouv.fr/api/v1/cavites"
    
    #Temporary layer for resulting polygons
    layerResult = QgsVectorLayer("Point?crs=epsg:"+str(srcProjSRID), "API Georisques - Cavités", "memory")
    layerResultProvider = layerResult.dataProvider()
    
    layerResultProvider.addAttributes([QgsField("code_insee",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("departement",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("identifiant",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("latitude",QVariant.Double, "double", 0)])
    layerResultProvider.addAttributes([QgsField("longitude",QVariant.Double, "double", 0)])
    layerResultProvider.addAttributes([QgsField("nom",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("region",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("reperage_geo",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("type",QVariant.String, "string", 0)])
    
    layerResult.updateFields()

    #list for insert
    
    listFieldsNames = [f.name() for f in layerResult.fields()]
    
    #list for insert
    
    listNewFeats = []
    
    #Get bounding box
    
    bbox = projectPerimeterGeom.boundingBox()
            
    xMin = bbox.xMinimum()
    xMax = bbox.xMaximum()
    yMin = bbox.yMinimum()
    yMax = bbox.yMaximum()

    #decompose extent rectangle by future request radius
    
    listX, listY = getListsXY(xMin,xMax,yMin,yMax,d10000)

    #Requests on Georisques API
    QgsMessageLog.logMessage("Requests on Georisques API - Cavités",tag="Data Processing", level=Qgis.Info)
    
    for xReq in listX :
        
        for yReq in listY :
            
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {
                'rayon' : requestDistance,
                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                if not data["data"] : continue
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    code_insee = currentResult["code_insee"] if ("code_insee" in currentResult.keys()) else None
                    departement = str(currentResult["departement"]) if ("departement" in currentResult.keys()) else None
                    identifiant = currentResult["identifiant"] if ("identifiant" in currentResult.keys()) else None
                    latitude = float(currentResult["latitude"]) if ("latitude" in currentResult.keys()) else None
                    longitude = float(currentResult["longitude"]) if ("longitude" in currentResult.keys()) else None
                    nom = currentResult["nom"] if ("nom" in currentResult.keys()) else None
                    region = str(currentResult["region"]) if ("region" in currentResult.keys()) else None
                    reperage_geo = currentResult["reperage_geo"] if ("reperage_geo" in currentResult.keys()) else None
                    type = currentResult["type"] if ("type" in currentResult.keys()) else None
                    commune = currentResult["commune"] if ("commune" in currentResult.keys()) else None
                        
                    newFeat = QgsFeature()
                    newFeat.setFields(layerResult.fields())
                    
                    ptResult = tform.transform(QgsPointXY(longitude, latitude),QgsCoordinateTransform.ReverseTransform)
                    newGeom = QgsGeometry.fromPointXY(ptResult)
                    
                    if processOption == 0 :
                        
                        if not newGeom.intersects(projectPerimeterGeom) : continue
                        
                    newFeat.setGeometry(newGeom)
                    
                    newFeat.setAttribute("code_insee",code_insee)
                    newFeat.setAttribute("departement",departement)
                    newFeat.setAttribute("identifiant",identifiant)
                    newFeat.setAttribute("latitude",latitude)
                    newFeat.setAttribute("longitude",longitude)
                    newFeat.setAttribute("nom",nom)
                    newFeat.setAttribute("region",region)
                    newFeat.setAttribute("reperage_geo",reperage_geo)
                    newFeat.setAttribute("type",type)                 
                    
                    listNewFeats.append(newFeat)

    listNewFeats = removeDuplicates(listNewFeats)

    layerResult.dataProvider().addFeatures(listNewFeats)
    layerResult.commitChanges()
    
    #End of nodes insertion; update temp layer extents

    layerResult.updateExtents()
    
    QgsProject.instance().addMapLayer(layerResult)
    
    del url, layerResult, layerResultProvider, listFieldsNames, listNewFeats
    del bbox, xMin, xMax, yMin, yMax, listX, listY
    
    try :
        del ptReq, ptReqT, params, formatted_url, response, data, currentResult, newFeat, newGeom
    except :
        pass
    
    try :
        del code_insee, departement, identifiant, latitude, longitude, nom, region, reperage_geo, type
    except :
        pass

def request_DICRIM(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000):
    
    # API url
        
    url = "https://www.georisques.gouv.fr/api/v1/gaspar/dicrim"
    
    
    #Temporary layer for resulting polygons
    layerResult = QgsVectorLayer("MultiPolygon?crs=epsg:"+str(srcProjSRID), "API Géorisques - DICRIM", "memory")
    layerResultProvider = layerResult.dataProvider()
    
    layerResultProvider.addAttributes([QgsField("annee_publication",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("code_insee",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_commune",QVariant.String, "string", 0)])
        
    layerResult.updateFields()
    
    listFieldsNames = [f.name() for f in layerResult.fields()]
    
    #list for insert
    
    listNewFeats = []
    
    #Get bounding box
    
    bbox = projectPerimeterGeom.boundingBox()
            
    xMin = bbox.xMinimum()
    xMax = bbox.xMaximum()
    yMin = bbox.yMinimum()
    yMax = bbox.yMaximum()

    #decompose extent rectangle by future request radius
    
    listX, listY = getListsXY(xMin,xMax,yMin,yMax,d10000)

    #Requests on Georisques API
    QgsMessageLog.logMessage("Requests on Georisques API - DICRIM",tag="Data Processing", level=Qgis.Info)
    
    for xReq in listX :
        
        for yReq in listY :
            
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {
                'rayon' : requestDistance,
                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                if not data["data"] : continue
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    annee_publication = currentResult["annee_publication"]
                    code_insee = currentResult["code_insee"]
                    libelle_commune = str(currentResult["libelle_commune"])
                    
                    code_insee_req = code_insee.zfill(5)
                    
                    newFeat = QgsFeature()
                    newFeat.setFields(layerResult.fields())
                    
                    newGeom = getMultiPolygonContourFromINSEE(code_insee_req,tform)
                    
                    if processOption == 0 :
                        
                        if not newGeom.intersects(projectPerimeterGeom) : continue
                        
                    newFeat.setGeometry(newGeom)
                    newFeat.setAttribute("annee_publication",annee_publication)
                    newFeat.setAttribute("code_insee",code_insee)
                    newFeat.setAttribute("libelle_commune",libelle_commune)
                    
                    listNewFeats.append(newFeat)

    listNewFeats = removeDuplicates(listNewFeats)
    
    layerResult.dataProvider().addFeatures(listNewFeats)
    layerResult.commitChanges()

    #End of nodes insertion; update temp layer extents

    layerResult.updateExtents()
    
    QgsProject.instance().addMapLayer(layerResult)
    
    del url, layerResult, layerResultProvider, listFieldsNames, listNewFeats
    del bbox, xMin, xMax, yMin, yMax, listX, listY
    
    try :
        del ptReq, ptReqT, params, formatted_url, response, data, currentResult, newFeat, newGeom
    except :
        pass
    
    try :
        del annee_publication, code_insee, libelle_commune
    except :
        pass
    
def request_Installations_Classees(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000):
    
    # API url
        
    url = "https://www.georisques.gouv.fr/api/v1/installations_classees"
    
    #Temporary layer for resulting polygons
    layerResult = QgsVectorLayer("Point?crs=epsg:"+str(srcProjSRID), "API Georisques - Installations classées", "memory")
    layerResultProvider = layerResult.dataProvider()
    
    layerResultProvider.addAttributes([QgsField("adresse1",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("adresse2",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("adresse3",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("bovins",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("carriere",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("codeAIOT",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("codeInsee",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("codeNaf",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("codePostal",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("commune",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("coordonneeXAIOT",QVariant.Double, "double", 0)])
    layerResultProvider.addAttributes([QgsField("coordonneeYAIOT",QVariant.Double, "double", 0)])
    layerResultProvider.addAttributes([QgsField("date_maj",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("documentsHorsInspection",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("eolienne",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("etatActivite",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("ied",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("industrie",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("inspections",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("latitude",QVariant.Double, "double", 0)])
    layerResultProvider.addAttributes([QgsField("longitude",QVariant.Double, "double", 0)])
    layerResultProvider.addAttributes([QgsField("porcs",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("prioriteNationale",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("raisonSociale",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("regime",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("rubriques",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("serviceAIOT",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("siret",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("statutSeveso",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("systemeCoordonneesAIOT",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("volailles",QVariant.String, "string", 0)])
    
    layerResult.updateFields()

    #list for insert
    
    listFieldsNames = [f.name() for f in layerResult.fields()]
    
    #list for insert
    
    listNewFeats = []
    
    #Get bounding box
    
    bbox = projectPerimeterGeom.boundingBox()
            
    xMin = bbox.xMinimum()
    xMax = bbox.xMaximum()
    yMin = bbox.yMinimum()
    yMax = bbox.yMaximum()

    #decompose extent rectangle by future request radius
    
    listX, listY = getListsXY(xMin,xMax,yMin,yMax,d10000)

    #Requests on Georisques API
    QgsMessageLog.logMessage("Requests on Georisques API - Installations classées",tag="Data Processing", level=Qgis.Info)
    
    for xReq in listX :
        
        for yReq in listY :
            
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {
                'rayon' : requestDistance,
                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                if not data["data"] : continue
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    adresse1 = currentResult["adresse1"] if ("adresse1" in currentResult.keys()) else None
                    adresse2 = currentResult["adresse2"] if ("adresse2" in currentResult.keys()) else None
                    adresse3 = currentResult["adresse3"] if ("adresse3" in currentResult.keys()) else None
                    bovins = str(currentResult["bovins"]) if ("bovins" in currentResult.keys()) else None
                    carriere = currentResult["carriere"] if ("carriere" in currentResult.keys()) else None
                    codeAIOT = currentResult["codeAIOT"] if ("codeAIOT" in currentResult.keys()) else None
                    codeInsee = currentResult["codeInsee"] if ("codeInsee" in currentResult.keys()) else None
                    codeNaf = currentResult["codeNaf"] if ("codeNaf" in currentResult.keys()) else None
                    codePostal = currentResult["codePostal"] if ("codePostal" in currentResult.keys()) else None
                    commune = currentResult["commune"] if ("commune" in currentResult.keys()) else None
                    coordonneeXAIOT = float(currentResult["coordonneeXAIOT"]) if ("coordonneeXAIOT" in currentResult.keys()) else None
                    coordonneeYAIOT = float(currentResult["coordonneeYAIOT"]) if ("coordonneeYAIOT" in currentResult.keys()) else None
                    date_maj = currentResult["date_maj"] if ("date_maj" in currentResult.keys()) else None
                    documentsHorsInspection = str(currentResult["documentsHorsInspection"]) if ("documentsHorsInspection" in currentResult.keys()) else None
                    eolienne = currentResult["eolienne"] if ("eolienne" in currentResult.keys()) else None
                    etatActivite = currentResult["etatActivite"] if ("etatActivite" in currentResult.keys()) else None
                    ied = currentResult["ied"] if ("ied" in currentResult.keys()) else None
                    etatActivite = currentResult["etatActivite"] if ("etatActivite" in currentResult.keys()) else None
                    industrie = currentResult["industrie"] if ("industrie" in currentResult.keys()) else None
                    inspections = str(currentResult["inspections"]) if ("inspections" in currentResult.keys()) else None
                    latitude = float(currentResult["latitude"]) if ("latitude" in currentResult.keys()) else None
                    longitude = float(currentResult["longitude"]) if ("longitude" in currentResult.keys()) else None
                    porcs = currentResult["porcs"] if ("porcs" in currentResult.keys()) else None
                    prioriteNationale = currentResult["prioriteNationale"] if ("prioriteNationale" in currentResult.keys()) else None
                    raisonSociale = currentResult["raisonSociale"] if ("raisonSociale" in currentResult.keys()) else None
                    regime = currentResult["regime"] if ("regime" in currentResult.keys()) else None
                    rubriques = str(currentResult["rubriques"]) if ("rubriques" in currentResult.keys()) else None
                    serviceAIOT = currentResult["serviceAIOT"] if ("serviceAIOT" in currentResult.keys()) else None
                    siret = currentResult["siret"] if ("siret" in currentResult.keys()) else None
                    statutSeveso = currentResult["statutSeveso"] if ("statutSeveso" in currentResult.keys()) else None
                    systemeCoordonneesAIOT = currentResult["systemeCoordonneesAIOT"] if ("systemeCoordonneesAIOT" in currentResult.keys()) else None
                    volailles = currentResult["volailles"] if ("volailles" in currentResult.keys()) else None
                        
                    newFeat = QgsFeature()
                    newFeat.setFields(layerResult.fields())
                    
                    ptResult = tform.transform(QgsPointXY(longitude, latitude),QgsCoordinateTransform.ReverseTransform)

                    newGeom = QgsGeometry.fromPointXY(ptResult)
                    
                    if processOption == 0 :
                        
                        if not newGeom.intersects(projectPerimeterGeom) : continue

                    newFeat.setGeometry(newGeom)
                    
                    newFeat.setAttribute("adresse1",adresse1)
                    newFeat.setAttribute("adresse2",adresse2)
                    newFeat.setAttribute("adresse3",adresse3)
                    newFeat.setAttribute("bovins",bovins)
                    newFeat.setAttribute("carriere",carriere)
                    newFeat.setAttribute("codeAIOT",codeAIOT)
                    newFeat.setAttribute("codeInsee",codeInsee)
                    newFeat.setAttribute("codeNaf",codeNaf)
                    newFeat.setAttribute("codePostal",codePostal)
                    newFeat.setAttribute("commune",commune)
                    newFeat.setAttribute("coordonneeXAIOT",coordonneeXAIOT)
                    newFeat.setAttribute("coordonneeYAIOT",coordonneeYAIOT)
                    newFeat.setAttribute("date_maj",date_maj)
                    newFeat.setAttribute("documentsHorsInspection",documentsHorsInspection)
                    newFeat.setAttribute("eolienne",eolienne)
                    newFeat.setAttribute("etatActivite",etatActivite)
                    newFeat.setAttribute("industrie",industrie)
                    newFeat.setAttribute("inspections",inspections)
                    newFeat.setAttribute("latitude",latitude)
                    newFeat.setAttribute("longitude",longitude)
                    newFeat.setAttribute("porcs",porcs)
                    newFeat.setAttribute("prioriteNationale",prioriteNationale)
                    newFeat.setAttribute("raisonSociale",raisonSociale)
                    newFeat.setAttribute("regime",regime)
                    newFeat.setAttribute("rubriques",rubriques)
                    newFeat.setAttribute("serviceAIOT",serviceAIOT)
                    newFeat.setAttribute("siret",siret)
                    newFeat.setAttribute("statutSeveso",statutSeveso)
                    newFeat.setAttribute("systemeCoordonneesAIOT",systemeCoordonneesAIOT)
                    newFeat.setAttribute("volailles",volailles)                   
                    
                    listNewFeats.append(newFeat)

    listNewFeats = removeDuplicates(listNewFeats)

    layerResult.dataProvider().addFeatures(listNewFeats)
    layerResult.commitChanges()

    #End of nodes insertion; update temp layer extents

    layerResult.updateExtents()
    
    QgsProject.instance().addMapLayer(layerResult)
    
    del url, layerResult, layerResultProvider, listFieldsNames, listNewFeats
    del bbox, xMin, xMax, yMin, yMax, listX, listY
    
    try :
        del ptReq, ptReqT, params, formatted_url, response, data, currentResult, newFeat, newGeom
    except :
        pass
    
    try :
        del adresse1, adresse2, adresse3, bovins, carriere, codeAIOT, codeInsee, codeNaf, codePostal, commune
        del coordonneeXAIOT, coordonneeYAIOT, date_maj, documentsHorsInspection, eolienne, etatActivite, industrie
        del inspections, latitude, longitude, porcs, prioriteNationale, raisonSociale, regime, rubriques, serviceAIOT
        del siret, statutSeveso, systemeCoordonneesAIOT, volailles
    except :
        pass
    
def request_MVT(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000):
    
    # API url
        
    url = "https://www.georisques.gouv.fr/api/v1/mvt"
    
    #Temporary layer for resulting polygons
    layerResult = QgsVectorLayer("Point?crs=epsg:"+str(srcProjSRID), "API Georisques - Mouvements de terrain", "memory")
    layerResultProvider = layerResult.dataProvider()
    
    layerResultProvider.addAttributes([QgsField("code_insee",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("commentaire_lieu",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("commentaire_mvt",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_debut",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_maj",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("departement",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("fiabilite",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("identifiant",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("latitude",QVariant.Double, "double", 0)])
    layerResultProvider.addAttributes([QgsField("lieu",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("longitude",QVariant.Double, "double", 0)])
    layerResultProvider.addAttributes([QgsField("precision_date",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("precision_lieu",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("region",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("type",QVariant.String, "string", 0)])
    
    layerResult.updateFields()

    #list for insert
    
    listFieldsNames = [f.name() for f in layerResult.fields()]
    
    #list for insert
    
    listNewFeats = []
    
    #Get bounding box
    
    bbox = projectPerimeterGeom.boundingBox()
            
    xMin = bbox.xMinimum()
    xMax = bbox.xMaximum()
    yMin = bbox.yMinimum()
    yMax = bbox.yMaximum()

    #decompose extent rectangle by future request radius
    
    listX, listY = getListsXY(xMin,xMax,yMin,yMax,d10000)

            
    #start process
    #get project extent

    #Requests on Georisques API
    QgsMessageLog.logMessage("Requests on Georisques API - Mouvements de terrain",tag="Data Processing", level=Qgis.Info)
    
    for xReq in listX :
        
        for yReq in listY :
            
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {
                'rayon' : requestDistance,
                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                if not data["data"] : continue
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    code_insee = currentResult["code_insee"] if ("code_insee" in currentResult.keys()) else None
                    commentaire_lieu = currentResult["commentaire_lieu"] if ("commentaire_lieu" in currentResult.keys()) else None
                    commentaire_mvt = currentResult["commentaire_mvt"] if ("commentaire_mvt" in currentResult.keys()) else None
                    date_debut = str(currentResult["date_debut"]) if ("date_debut" in currentResult.keys()) else None
                    date_maj = currentResult["date_maj"] if ("date_maj" in currentResult.keys()) else None
                    departement = str(currentResult["departement"]) if ("departement" in currentResult.keys()) else None
                    fiabilite = currentResult["fiabilite"] if ("fiabilite" in currentResult.keys()) else None
                    identifiant = currentResult["identifiant"] if ("identifiant" in currentResult.keys()) else None
                    latitude = float(currentResult["latitude"]) if ("latitude" in currentResult.keys()) else None
                    lieu = currentResult["lieu"] if ("lieu" in currentResult.keys()) else None
                    longitude = float(currentResult["longitude"]) if ("longitude" in currentResult.keys()) else None
                    precision_date = currentResult["precision_date"] if ("precision_date" in currentResult.keys()) else None
                    precision_lieu = currentResult["precision_lieu"] if ("precision_lieu" in currentResult.keys()) else None
                    region = str(currentResult["region"]) if ("region" in currentResult.keys()) else None
                    typecr = currentResult["type"] if ("type" in currentResult.keys()) else None
                        
                    newFeat = QgsFeature()
                    newFeat.setFields(layerResult.fields())
                    
                    ptResult = tform.transform(QgsPointXY(longitude, latitude),QgsCoordinateTransform.ReverseTransform)
                    newGeom = QgsGeometry.fromPointXY(ptResult)
                    
                    if processOption == 0 :
                        
                        if not newGeom.intersects(projectPerimeterGeom) : continue
                        
                    newFeat.setGeometry(newGeom)
                    
                    newFeat.setAttribute("code_insee",code_insee)
                    newFeat.setAttribute("commentaire_lieu",commentaire_lieu)
                    newFeat.setAttribute("commentaire_mvt",commentaire_mvt)
                    newFeat.setAttribute("date_debut",date_debut)
                    newFeat.setAttribute("date_maj",date_maj)
                    newFeat.setAttribute("departement",departement)
                    newFeat.setAttribute("fiabilite",fiabilite)
                    newFeat.setAttribute("identifiant",identifiant)
                    newFeat.setAttribute("latitude",latitude)
                    newFeat.setAttribute("lieu",lieu)
                    newFeat.setAttribute("longitude",longitude)
                    newFeat.setAttribute("precision_date",precision_date)
                    newFeat.setAttribute("precision_lieu",precision_lieu)
                    newFeat.setAttribute("region",region)
                    newFeat.setAttribute("type",typecr)             
                    
                    listNewFeats.append(newFeat)

    listNewFeats = removeDuplicates(listNewFeats)

    layerResult.dataProvider().addFeatures(listNewFeats)
    layerResult.commitChanges()
    
    #End of nodes insertion; update temp layer extents

    layerResult.updateExtents()
    
    QgsProject.instance().addMapLayer(layerResult)
    
    del url, layerResult, layerResultProvider, listFieldsNames, listNewFeats
    del bbox, xMin, xMax, yMin, yMax, listX, listY
    
    try :
        del ptReq, ptReqT, params, formatted_url, response, data, currentResult, newFeat, newGeom
    except :
        pass
    
    try :
        del code_insee, commentaire_lieu, commentaire_mvt, date_debut,  date_maj, departement
        del fiabilite, identifiant, latitude, lieu, longitude, precision_date, precision_lieu
        del region, typecr
    except :
        pass
   
def request_PAPI(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000):
    
    # API url
        
    url = "https://www.georisques.gouv.fr/api/v1/gaspar/papi"
    
    #Temporary layer for resulting polygons
    layerResult = QgsVectorLayer("MultiPolygon?crs=epsg:"+str(srcProjSRID), "API Géorisques - PAPI", "memory")
    layerResultProvider = layerResult.dataProvider()
    
    layerResultProvider.addAttributes([QgsField("code_insee",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("code_national_papi",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_fin_realisation",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_labellisation",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_signature",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_bassin_risques",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_commentaire",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_commune",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_papi",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("liste_libelle_risque",QVariant.String, "string", 0)])
    
    
    layerResult.updateFields()

    #list for insert
    
    listFieldsNames = [f.name() for f in layerResult.fields()]
    
    #list for insert
    
    listNewFeats = []
    
    #Get bounding box
    
    bbox = projectPerimeterGeom.boundingBox()
            
    xMin = bbox.xMinimum()
    xMax = bbox.xMaximum()
    yMin = bbox.yMinimum()
    yMax = bbox.yMaximum()

    #decompose extent rectangle by future request radius
    
    listX, listY = getListsXY(xMin,xMax,yMin,yMax,d10000)
    
    for xReq in listX :
        
        for yReq in listY :
            
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {
                'rayon' : requestDistance,
                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                if not data["data"] : continue
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    code_insee = currentResult["code_insee"]
                    code_national_papi = currentResult["code_national_papi"]
                    date_fin_realisation = currentResult["date_fin_realisation"]
                    date_labellisation = str(currentResult["date_labellisation"])
                    date_signature = currentResult["date_signature"]
                    libelle_bassin_risques = currentResult["libelle_bassin_risques"]
                    libelle_commune = currentResult["libelle_commune"]
                    libelle_papi = currentResult["libelle_papi"]
                    liste_libelle_risque = str(currentResult["liste_libelle_risque"])
                        
                    newFeat = QgsFeature()
                    newFeat.setFields(layerResult.fields())
                    
                    newGeom = getMultiPolygonContourFromINSEE(code_insee,tform)
                    
                    if processOption == 0 :
                    
                        if not newGeom.intersects(projectPerimeterGeom) : continue
                        
                    newFeat.setGeometry(newGeom)
                    newFeat.setAttribute("code_insee",code_insee)
                    newFeat.setAttribute("code_national_papi",code_national_papi)
                    newFeat.setAttribute("date_fin_realisation",date_fin_realisation)
                    newFeat.setAttribute("date_labellisation",date_labellisation)
                    newFeat.setAttribute("date_signature",date_signature)
                    newFeat.setAttribute("libelle_bassin_risques",libelle_bassin_risques)
                    newFeat.setAttribute("libelle_commune",libelle_commune)
                    newFeat.setAttribute("libelle_papi",libelle_papi)
                    newFeat.setAttribute("liste_libelle_risque",liste_libelle_risque)
                    
                    listNewFeats.append(newFeat)

    listNewFeats = removeDuplicates(listNewFeats)
    
    layerResult.dataProvider().addFeatures(listNewFeats)

    #End of nodes insertion; update temp layer extents

    layerResult.updateExtents()
    
    QgsProject.instance().addMapLayer(layerResult)
    
    del url, layerResult, layerResultProvider, listFieldsNames, listNewFeats
    del bbox, xMin, xMax, yMin, yMax, listX, listY
    
    try :
        del ptReq, ptReqT, params, formatted_url, response, data, currentResult, newFeat, newGeom
    except :
        pass
    
    try :
        del code_insee, code_national_papi, date_fin_realisation, date_labellisation, date_signature
        del libelle_bassin_risques, libelle_commune, libelle_papi, liste_libelle_risque
    except :
        pass

def request_PCS(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000):
    
    # API url
        
    url = "https://www.georisques.gouv.fr/api/v1/gaspar/pcs"
    
    
    #Temporary layer for resulting polygons
    layerResult = QgsVectorLayer("MultiPolygon?crs=epsg:"+str(srcProjSRID), "API Géorisques - PCS", "memory")
    layerResultProvider = layerResult.dataProvider()
    
    layerResultProvider.addAttributes([QgsField("code_insee",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("code_national_pcs",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_debut_etude",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_fin_etude",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_notification",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_bassin_risques",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_commentaire",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_commune",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_pcs",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("liste_libelle_risque",QVariant.String, "string", 0)])
        
    layerResult.updateFields()
    
    listFieldsNames = [f.name() for f in layerResult.fields()]
    
    #list for insert
    
    listNewFeats = []
    
    #Get bounding box
    
    bbox = projectPerimeterGeom.boundingBox()
            
    xMin = bbox.xMinimum()
    xMax = bbox.xMaximum()
    yMin = bbox.yMinimum()
    yMax = bbox.yMaximum()

    #decompose extent rectangle by future request radius
    
    listX, listY = getListsXY(xMin,xMax,yMin,yMax,d10000)

    #Requests on Georisques API
    QgsMessageLog.logMessage("Requests on Georisques API - PCS",tag="Data Processing", level=Qgis.Info)
    
    for xReq in listX :
        
        for yReq in listY :
            
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {
                'rayon' : requestDistance,
                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                if not data["data"] : continue
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    code_insee = currentResult["code_insee"]
                    code_national_pcs = currentResult["code_national_pcs"]
                    date_debut_etude = currentResult["date_debut_etude"]
                    date_fin_etude = currentResult["date_fin_etude"]
                    date_notification = currentResult["date_notification"]
                    libelle_bassin_risques = currentResult["libelle_bassin_risques"]
                    libelle_commentaire = currentResult["libelle_commentaire"]
                    libelle_commune = currentResult["libelle_commune"]
                    libelle_pcs = currentResult["libelle_pcs"]
                    liste_libelle_risque = str(currentResult["liste_libelle_risque"])
                    
                    code_insee_req = code_insee.zfill(5)
                    
                    newFeat = QgsFeature()
                    newFeat.setFields(layerResult.fields())
                    
                    newGeom = getMultiPolygonContourFromINSEE(code_insee_req,tform)
                    
                    if processOption == 0 :
                        
                        if not newGeom.intersects(projectPerimeterGeom) : continue
                        
                    newFeat.setGeometry(newGeom)
                    newFeat.setAttribute("code_insee",code_insee)
                    newFeat.setAttribute("code_national_pcs",code_national_pcs)
                    newFeat.setAttribute("date_debut_etude",date_debut_etude)
                    newFeat.setAttribute("date_fin_etude",date_fin_etude)
                    newFeat.setAttribute("date_notification",date_notification)
                    newFeat.setAttribute("libelle_bassin_risques",libelle_bassin_risques)
                    newFeat.setAttribute("libelle_commentaire",libelle_commentaire)
                    newFeat.setAttribute("libelle_commune",libelle_commune)
                    newFeat.setAttribute("libelle_pcs",libelle_pcs)
                    newFeat.setAttribute("liste_libelle_risque",liste_libelle_risque)
                    
                    listNewFeats.append(newFeat)

    listNewFeats = removeDuplicates(listNewFeats)
    
    layerResult.dataProvider().addFeatures(listNewFeats)
    layerResult.commitChanges()
    
    #End of nodes insertion; update temp layer extents

    layerResult.updateExtents()
    
    QgsProject.instance().addMapLayer(layerResult)
    
    del url, layerResult, layerResultProvider, listFieldsNames, listNewFeats
    del bbox, xMin, xMax, yMin, yMax, listX, listY
    
    try :
        del ptReq, ptReqT, params, formatted_url, response, data, currentResult, newFeat, newGeom
    except :
        pass
    
    try :
        del code_insee, code_national_pcs, date_debut_etude, date_fin_etude, date_notification
        del libelle_bassin_risques, libelle_commentaire, libelle_commune, libelle_pcs, liste_libelle_risque
    except :
        pass

def request_PPR(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000):
    
    # API url
        
    url = "https://www.georisques.gouv.fr/api/v1/ppr"
    
    #Temporary layer for resulting polygons
    layerResult = QgsVectorLayer("MultiPolygon?crs=epsg:"+str(srcProjSRID), "API Géorisques - Périmètres PPR", "memory")
    layerResultProvider = layerResult.dataProvider()
    
    layerResultProvider.addAttributes([QgsField("code_etat",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_etat",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("id_gaspar",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("risque",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_commune",QVariant.String, "string", 0)])
    
    layerResult.updateFields()

    #list for insert
    
    listFieldsNames = [f.name() for f in layerResult.fields()]
    
    #list for insert
    
    listNewFeats = []
    
    #Get bounding box
    
    bbox = projectPerimeterGeom.boundingBox()
            
    xMin = bbox.xMinimum()
    xMax = bbox.xMaximum()
    yMin = bbox.yMinimum()
    yMax = bbox.yMaximum()

    #decompose extent rectangle by future request radius
    
    listX, listY = getListsXY(xMin,xMax,yMin,yMax,d10000)
        
    #start process
    #get project extent

    #Requests on Georisques API
    QgsMessageLog.logMessage("Requests on Georisques API - PPR",tag="Data Processing", level=Qgis.Info)
    
    for xReq in listX :
        
        for yReq in listY :
            
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {
                'rayon' : requestDistance,
                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                if not data["data"] : continue
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    code_etat = currentResult["etat"]["code_etat"]
                    libelle_etat = currentResult["etat"]["libelle_etat"]
                    id_gaspar = currentResult["id_gaspar"]
                    risque = str(currentResult["risque"])
                    libelle_commune = currentResult["libelle_commune"]
                    features = currentResult["geom_perimetre"]["features"]
                    
                    for i_feature in range(len(features)) :
                        
                        newFeat = QgsFeature()
                        newFeat.setFields(layerResult.fields())
    
                        newGeom = QgsGeometry()
                        
                        coordinates = features[i_feature]["geometry"]["coordinates"]
                        
                        listParts = []
                        
                        for i_coordinates in range(len(coordinates)) :
                                
                            len2 = len(coordinates[i_coordinates])
                            
                            for i2 in range(len2) :
                                
                                listCoordinates = coordinates[i_coordinates][i2]
                                
                                listNewPart = []
                                
                                for coords in listCoordinates :
                                    
                                    long = coords[0]
                                    lat = coords[1]
                                    
                                    ptRev = QgsGeometry.fromPointXY(QgsPointXY(long, lat))
                                    ptRevT = tform.transform(ptRev.asPoint(),QgsCoordinateTransform.ReverseTransform)
                                    
                                    listNewPart.append(QgsPointXY(ptRevT.x(),ptRevT.y()))
                                    
                                listParts.append(listNewPart)

                        newGeom = QgsGeometry.fromMultiPolygonXY([listParts])
                        
                        if processOption == 0 :
                        
                            if not newGeom.intersects(projectPerimeterGeom) : continue
                            
                        newFeat.setGeometry(newGeom)
                        newFeat.setAttribute("code_etat",code_etat)
                        newFeat.setAttribute("libelle_etat",libelle_etat)
                        newFeat.setAttribute("id_gaspar",id_gaspar)
                        newFeat.setAttribute("risque",risque)
                        newFeat.setAttribute("libelle_commune",libelle_commune)
                        
                        listNewFeats.append(newFeat)

    listNewFeats = removeDuplicates(listNewFeats)

    layerResult.dataProvider().addFeatures(listNewFeats)
    layerResult.commitChanges()

    #End of nodes insertion; update temp layer extents

    layerResult.updateExtents()
    
    QgsProject.instance().addMapLayer(layerResult)
    
    del url, layerResult, layerResultProvider, listFieldsNames, listNewFeats
    del bbox, xMin, xMax, yMin, yMax, listX, listY
    
    try :
        del ptReq, ptReqT, params, formatted_url, response, data, currentResult, newFeat, newGeom
    except :
        pass
    
    try :
        del code_etat, libelle_etat, id_gaspar, risque, libelle_commune
    except :
        pass

def request_Radon(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000):
    
    # API url
        
    url = "https://www.georisques.gouv.fr/api/v1/radon"
    
    
    #Temporary layer for resulting polygons
    layerResult = QgsVectorLayer("MultiPolygon?crs=epsg:"+str(srcProjSRID), "API Géorisques - Radon", "memory")
    layerResultProvider = layerResult.dataProvider()
    
    layerResultProvider.addAttributes([QgsField("classe_potentiel",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("code_insee",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_commune",QVariant.String, "string", 0)])
        
    layerResult.updateFields()
    
    listFieldsNames = [f.name() for f in layerResult.fields()]
    
    #list for insert
    
    listNewFeats = []
    
    #Get bounding box
    
    bbox = projectPerimeterGeom.boundingBox()
            
    xMin = bbox.xMinimum()
    xMax = bbox.xMaximum()
    yMin = bbox.yMinimum()
    yMax = bbox.yMaximum()

    #decompose extent rectangle by future request radius
    
    listX, listY = getListsXY(xMin,xMax,yMin,yMax,d10000)

    #Requests on Georisques API
    QgsMessageLog.logMessage("Requests on Georisques API - Radon",tag="Data Processing", level=Qgis.Info)
    
    for xReq in listX :
        
        for yReq in listY :
            
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())
            
            inseeAtLonLat = getINSEECodeFromLonLat(ptReqT.x(),ptReqT.y())

            params = {
                'code_insee' : str(inseeAtLonLat)
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                if not data["data"] : continue
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    classe_potentiel = currentResult["classe_potentiel"]
                    code_insee = currentResult["code_insee"]
                    libelle_commune = currentResult["libelle_commune"]
                    
                    code_insee_req = code_insee.zfill(5)
                    
                    newFeat = QgsFeature()
                    newFeat.setFields(layerResult.fields())
                    
                    newGeom = getMultiPolygonContourFromINSEE(code_insee_req,tform)
                    
                    if processOption == 0 :
                        
                        if not newGeom.intersects(projectPerimeterGeom) : continue
                        
                    newFeat.setGeometry(newGeom)
                    newFeat.setAttribute("classe_potentiel",classe_potentiel)
                    newFeat.setAttribute("code_insee",code_insee)
                    newFeat.setAttribute("libelle_commune",libelle_commune)
                    
                    listNewFeats.append(newFeat)

    listNewFeats = removeDuplicates(listNewFeats)
    
    layerResult.dataProvider().addFeatures(listNewFeats)
    layerResult.commitChanges()

    #End of nodes insertion; update temp layer extents

    layerResult.updateExtents()
    
    QgsProject.instance().addMapLayer(layerResult)
    
    del url, layerResult, layerResultProvider, listFieldsNames, listNewFeats
    del bbox, xMin, xMax, yMin, yMax, listX, listY
    
    try :
        del ptReq, ptReqT, params, formatted_url, response, data, currentResult, newFeat, newGeom
    except :
        pass
    
    try :
        del classe_potentiel, code_insee, libelle_commune
    except :
        pass

def request_Risques(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000):
    
    # API url
        
    url = "https://www.georisques.gouv.fr/api/v1/gaspar/risques"
    
    
    #Temporary layer for resulting polygons
    layerResult = QgsVectorLayer("MultiPolygon?crs=epsg:"+str(srcProjSRID), "API Géorisques - Risques", "memory")
    layerResultProvider = layerResult.dataProvider()
    
    layerResultProvider.addAttributes([QgsField("code_insee",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_commune",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("risques_detail",QVariant.String, "string", 0)])
        
    layerResult.updateFields()
    
    listFieldsNames = [f.name() for f in layerResult.fields()]
    
    #list for insert
    
    listNewFeats = []
    
    #Get bounding box
    
    bbox = projectPerimeterGeom.boundingBox()
            
    xMin = bbox.xMinimum()
    xMax = bbox.xMaximum()
    yMin = bbox.yMinimum()
    yMax = bbox.yMaximum()

    #decompose extent rectangle by future request radius
    
    listX, listY = getListsXY(xMin,xMax,yMin,yMax,d10000)

    #Requests on Georisques API
    QgsMessageLog.logMessage("Requests on Georisques API - Risques",tag="Data Processing", level=Qgis.Info)
    
    for xReq in listX :
        
        for yReq in listY :
            
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {
                'rayon' : requestDistance,
                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                if not data["data"] : continue
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    code_insee = currentResult["code_insee"]
                    libelle_commune = currentResult["libelle_commune"]
                    risques_detail = str(currentResult["risques_detail"])
                        
                    newFeat = QgsFeature()
                    newFeat.setFields(layerResult.fields())
                    
                    newGeom = getMultiPolygonContourFromINSEE(code_insee,tform)
                    
                    if processOption == 0 :
                    
                        if not newGeom.intersects(projectPerimeterGeom) : continue
                        
                    newFeat.setGeometry(newGeom)
                    newFeat.setAttribute("code_insee",code_insee)
                    newFeat.setAttribute("libelle_commune",libelle_commune)
                    newFeat.setAttribute("risques_detail",risques_detail)
                    
                    listNewFeats.append(newFeat)

    listNewFeats = removeDuplicates(listNewFeats)
    
    layerResult.dataProvider().addFeatures(listNewFeats)
    layerResult.commitChanges()

    #End of nodes insertion; update temp layer extents

    layerResult.updateExtents()
    
    QgsProject.instance().addMapLayer(layerResult)
    
    del url, layerResult, layerResultProvider, listFieldsNames, listNewFeats
    del bbox, xMin, xMax, yMin, yMax, listX, listY
    
    try :
        del ptReq, ptReqT, params, formatted_url, response, data, currentResult, newFeat, newGeom
    except :
        pass
    
    try :
        del code_insee, libelle_commune, risques_detail
    except :
        pass
    
def request_SIS(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000):
    
    # API url
        
    url = "https://www.georisques.gouv.fr/api/v1/sis"
    
    
    #Temporary layer for resulting polygons
    layerResult = QgsVectorLayer("MultiPolygon?crs=epsg:"+str(srcProjSRID), "API Géorisques - SIS", "memory")
    layerResultProvider = layerResult.dataProvider()
    
    layerResultProvider.addAttributes([QgsField("adresse",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("adresse_lieudit",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("code_insee",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("fiche_risque",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("geom",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("id_sis",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("nom",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("nom_commune",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("superficie",QVariant.String, "string", 0)])
        
    layerResult.updateFields()
    
    listFieldsNames = [f.name() for f in layerResult.fields()]
    
    #list for insert
    
    listNewFeats = []
    
    #Get bounding box
    
    bbox = projectPerimeterGeom.boundingBox()
            
    xMin = bbox.xMinimum()
    xMax = bbox.xMaximum()
    yMin = bbox.yMinimum()
    yMax = bbox.yMaximum()

    #decompose extent rectangle by future request radius
    
    listX, listY = getListsXY(xMin,xMax,yMin,yMax,d10000)

    #Requests on Georisques API
    QgsMessageLog.logMessage("Requests on Georisques API - SIS",tag="Data Processing", level=Qgis.Info)
    
    for xReq in listX :
        
        for yReq in listY :
            
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {
                'rayon' : requestDistance,
                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                if not data["data"] : continue
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    adresse = str(currentResult["adresse"])
                    adresse_lieudit = currentResult["adresse_lieudit"]
                    code_insee = str(currentResult["code_insee"])
                    fiche_risque = str(currentResult["fiche_risque"])
                    geom = currentResult["geom"]
                    id_sis = str(currentResult["id_sis"])
                    nom = str(currentResult["nom"])
                    nom_commune = str(currentResult["nom_commune"])
                    superficie = str(currentResult["superficie"])
                        
                    newFeat = QgsFeature()
                    newFeat.setFields(layerResult.fields())
                    
                    QgsMessageLog.logMessage("code_insee "+str(code_insee),tag="Data Processing", level=Qgis.Info)
                    
                    coordinates = geom["coordinates"]
                        
                    listParts = []
                    
                    for i_coordinates in range(len(coordinates)) :
                            
                        len2 = len(coordinates[i_coordinates])
                        
                        for i2 in range(len2) :
                            
                            listCoordinates = coordinates[i_coordinates][i2]
                            
                            listNewPart = []
                            
                            for coords in listCoordinates :
                                
                                long = coords[0]
                                lat = coords[1]
                                
                                ptRev = QgsGeometry.fromPointXY(QgsPointXY(long, lat))
                                ptRevT = tform.transform(ptRev.asPoint(),QgsCoordinateTransform.ReverseTransform)
                                
                                listNewPart.append(QgsPointXY(ptRevT.x(),ptRevT.y()))
                                
                            listParts.append(listNewPart)

                    newGeom = QgsGeometry.fromMultiPolygonXY([listParts])
                    
                    # newGeom = getMultiPolygonContourFromINSEE(code_insee,tform)
                    
                    if processOption == 0 :
                    
                        if not newGeom.intersects(projectPerimeterGeom) : continue
                        
                    newFeat.setGeometry(newGeom)
                    newFeat.setAttribute("adresse",adresse)
                    newFeat.setAttribute("adresse_lieudit",adresse_lieudit)
                    newFeat.setAttribute("code_insee",code_insee)
                    newFeat.setAttribute("fiche_risque",fiche_risque)
                    newFeat.setAttribute("geom",str(geom))
                    newFeat.setAttribute("id_sis",id_sis)
                    newFeat.setAttribute("nom",nom)
                    newFeat.setAttribute("nom_commune",nom_commune)
                    newFeat.setAttribute("superficie",superficie)
                    
                    listNewFeats.append(newFeat)

    listNewFeats = removeDuplicates(listNewFeats)
    
    layerResult.dataProvider().addFeatures(listNewFeats)
    layerResult.commitChanges()

    #End of nodes insertion; update temp layer extents

    layerResult.updateExtents()
    
    QgsProject.instance().addMapLayer(layerResult)
    
    del url, layerResult, layerResultProvider, listFieldsNames, listNewFeats
    del bbox, xMin, xMax, yMin, yMax, listX, listY
    
    try :
        del ptReq, ptReqT, params, formatted_url, response, data, currentResult, newFeat, newGeom
    except :
        pass
    
    try :
        del adresse, adresse_lieudit, code_insee, fiche_risque, geom, id_sis, nom, nom_commune, superficie
    except :
        pass

def request_TIM(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000):
    
    # API url
        
    url = "https://www.georisques.gouv.fr/api/v1/gaspar/tim"
    
    
    #Temporary layer for resulting polygons
    layerResult = QgsVectorLayer("MultiPolygon?crs=epsg:"+str(srcProjSRID), "API Géorisques - TIM", "memory")
    layerResultProvider = layerResult.dataProvider()
    
    layerResultProvider.addAttributes([QgsField("code_insee",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_transmission",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_commune",QVariant.String, "string", 0)])
        
    layerResult.updateFields()
    
    listFieldsNames = [f.name() for f in layerResult.fields()]
    
    #list for insert
    
    listNewFeats = []
    
    #Get bounding box
    
    bbox = projectPerimeterGeom.boundingBox()
            
    xMin = bbox.xMinimum()
    xMax = bbox.xMaximum()
    yMin = bbox.yMinimum()
    yMax = bbox.yMaximum()

    #decompose extent rectangle by future request radius
    
    listX, listY = getListsXY(xMin,xMax,yMin,yMax,d10000)

    #Requests on Georisques API
    QgsMessageLog.logMessage("Requests on Georisques API - TIM",tag="Data Processing", level=Qgis.Info)
    
    for xReq in listX :
        
        for yReq in listY :
            
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {
                'rayon' : requestDistance,
                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                if not data["data"] : continue
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    code_insee = currentResult["code_insee"]
                    date_transmission = currentResult["date_transmission"]
                    libelle_commune = str(currentResult["libelle_commune"])
                        
                    newFeat = QgsFeature()
                    newFeat.setFields(layerResult.fields())
                    
                    newGeom = getMultiPolygonContourFromINSEE(code_insee,tform)
                    
                    if processOption == 0 :
                    
                        if not newGeom.intersects(projectPerimeterGeom) : continue
                        
                    newFeat.setGeometry(newGeom)
                    newFeat.setAttribute("code_insee",code_insee)
                    newFeat.setAttribute("date_transmission",date_transmission)
                    newFeat.setAttribute("libelle_commune",libelle_commune)
                    
                    listNewFeats.append(newFeat)

    listNewFeats = removeDuplicates(listNewFeats)
    
    layerResult.dataProvider().addFeatures(listNewFeats)
    layerResult.commitChanges()

    #End of nodes insertion; update temp layer extents

    layerResult.updateExtents()
    
    QgsProject.instance().addMapLayer(layerResult)
    
    del url, layerResult, layerResultProvider, listFieldsNames, listNewFeats
    del bbox, xMin, xMax, yMin, yMax, listX, listY
    
    try :
        del ptReq, ptReqT, params, formatted_url, response, data, currentResult, newFeat, newGeom
    except :
        pass
    
    try :
        del code_insee, date_transmission, libelle_commune
    except :
        pass

def request_TRI(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000):
    
    # API url
        
    url = "https://www.georisques.gouv.fr/api/v1/gaspar/tri"
    
    
    #Temporary layer for resulting polygons
    layerResult = QgsVectorLayer("MultiPolygon?crs=epsg:"+str(srcProjSRID), "API Géorisques - TRI", "memory")
    layerResultProvider = layerResult.dataProvider()
    
    layerResultProvider.addAttributes([QgsField("code_insee",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("code_national_tri",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_arrete_approbation",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_arrete_carte",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_arrete_national",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_arrete_pcb",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_arrete_pcb_local",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("date_arrete_prefet_parties_prenantes",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_bassin_risques",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_commentaire",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_commune",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_tri",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("liste_libelle_risque",QVariant.String, "string", 0)])
        
    layerResult.updateFields()
    
    listFieldsNames = [f.name() for f in layerResult.fields()]
    
    #list for insert
    
    listNewFeats = []
    
    #Get bounding box
    
    bbox = projectPerimeterGeom.boundingBox()
            
    xMin = bbox.xMinimum()
    xMax = bbox.xMaximum()
    yMin = bbox.yMinimum()
    yMax = bbox.yMaximum()

    #decompose extent rectangle by future request radius
    
    listX, listY = getListsXY(xMin,xMax,yMin,yMax,d10000)

    #Requests on Georisques API
    QgsMessageLog.logMessage("Requests on Georisques API - TRI",tag="Data Processing", level=Qgis.Info)
    
    for xReq in listX :
        
        for yReq in listY :
            
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {
                'rayon' : requestDistance,
                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                if not data["data"] : continue
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    code_insee = currentResult["code_insee"]
                    code_national_tri = currentResult["code_national_tri"]
                    date_arrete_approbation = currentResult["date_arrete_approbation"]
                    date_arrete_carte = currentResult["date_arrete_carte"]
                    date_arrete_national = currentResult["date_arrete_national"]
                    date_arrete_pcb = currentResult["date_arrete_pcb"]
                    date_arrete_pcb_local = currentResult["date_arrete_pcb_local"]
                    date_arrete_prefet_parties_prenantes = currentResult["date_arrete_prefet_parties_prenantes"]
                    libelle_bassin_risques = currentResult["libelle_bassin_risques"]
                    libelle_commentaire = currentResult["libelle_commentaire"]
                    libelle_commune = currentResult["libelle_commune"]
                    libelle_tri = currentResult["libelle_tri"]
                    liste_libelle_risque = str(currentResult["liste_libelle_risque"])
                        
                    newFeat = QgsFeature()
                    newFeat.setFields(layerResult.fields())
                    
                    newGeom = getMultiPolygonContourFromINSEE(code_insee,tform)
                    
                    if processOption == 0 :
                    
                        if not newGeom.intersects(projectPerimeterGeom) : continue
                        
                    newFeat.setGeometry(newGeom)
                    newFeat.setAttribute("code_insee",code_insee)
                    newFeat.setAttribute("code_national_tri",code_national_tri)
                    newFeat.setAttribute("date_arrete_approbation",date_arrete_approbation)
                    newFeat.setAttribute("date_arrete_carte",date_arrete_carte)
                    newFeat.setAttribute("date_arrete_national",date_arrete_national)
                    newFeat.setAttribute("date_arrete_pcb",date_arrete_pcb)
                    newFeat.setAttribute("date_arrete_pcb_local",date_arrete_pcb_local)
                    newFeat.setAttribute("date_arrete_prefet_parties_prenantes",date_arrete_prefet_parties_prenantes)
                    newFeat.setAttribute("libelle_bassin_risques",libelle_bassin_risques)
                    newFeat.setAttribute("libelle_commentaire",libelle_commentaire)
                    newFeat.setAttribute("libelle_commune",libelle_commune)
                    newFeat.setAttribute("libelle_tri",libelle_tri)
                    newFeat.setAttribute("liste_libelle_risque",liste_libelle_risque)
                    
                    listNewFeats.append(newFeat)

    listNewFeats = removeDuplicates(listNewFeats)
    
    layerResult.dataProvider().addFeatures(listNewFeats)
    layerResult.commitChanges()

    #End of nodes insertion; update temp layer extents

    layerResult.updateExtents()
    
    QgsProject.instance().addMapLayer(layerResult)
    
    del url, layerResult, layerResultProvider, listFieldsNames, listNewFeats
    del bbox, xMin, xMax, yMin, yMax, listX, listY
    
    try :
        del ptReq, ptReqT, params, formatted_url, response, data, currentResult, newFeat, newGeom
    except :
        pass
    
    try :
        del code_insee, code_national_tri, date_arrete_approbation, date_arrete_carte, date_arrete_national
        del date_arrete_pcb, date_arrete_pcb_local, date_arrete_prefet_parties_prenantes, libelle_bassin_risques
        del libelle_commentaire, libelle_commune, libelle_tri, liste_libelle_risque
    except :
        pass
    
def request_Zonage_Sismique(projectPerimeterGeom,srcProjSRID,tform,processOption,d10000):
    
    # API url
        
    url = "https://www.georisques.gouv.fr/api/v1/zonage_sismique"
    
    
    #Temporary layer for resulting polygons
    layerResult = QgsVectorLayer("MultiPolygon?crs=epsg:"+str(srcProjSRID), "API Géorisques - Zonage sismique", "memory")
    layerResultProvider = layerResult.dataProvider()
    
    layerResultProvider.addAttributes([QgsField("code_insee",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("code_zone",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("libelle_commune",QVariant.String, "string", 0)])
    layerResultProvider.addAttributes([QgsField("zone_sismicite",QVariant.String, "string", 0)])
        
    layerResult.updateFields()
    
    listFieldsNames = [f.name() for f in layerResult.fields()]
    
    #list for insert
    
    listNewFeats = []
    
    #Get bounding box
    
    bbox = projectPerimeterGeom.boundingBox()
            
    xMin = bbox.xMinimum()
    xMax = bbox.xMaximum()
    yMin = bbox.yMinimum()
    yMax = bbox.yMaximum()

    #decompose extent rectangle by future request radius
    
    listX, listY = getListsXY(xMin,xMax,yMin,yMax,d10000)

    #Requests on Georisques API
    QgsMessageLog.logMessage("Requests on Georisques API - Zonage sismique",tag="Data Processing", level=Qgis.Info)
    
    for xReq in listX :
        
        for yReq in listY :
            
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {
                'rayon' : requestDistance,
                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                if not data["data"] : continue
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    code_insee = currentResult["code_insee"]
                    code_zone = currentResult["code_zone"]
                    libelle_commune = currentResult["libelle_commune"]
                    zone_sismicite = currentResult["zone_sismicite"]  
                        
                    newFeat = QgsFeature()
                    newFeat.setFields(layerResult.fields())
                    
                    newGeom = getMultiPolygonContourFromINSEE(code_insee,tform)
                    
                    if processOption == 0 :
                    
                        if not newGeom.intersects(projectPerimeterGeom) : continue
                        
                    newFeat.setGeometry(newGeom)
                    newFeat.setAttribute("code_insee",code_insee)
                    newFeat.setAttribute("code_zone",code_zone)
                    newFeat.setAttribute("libelle_commune",libelle_commune)
                    newFeat.setAttribute("zone_sismicite",zone_sismicite)
                    
                    listNewFeats.append(newFeat)

    listNewFeats = removeDuplicates(listNewFeats)
    
    layerResult.dataProvider().addFeatures(listNewFeats)
    layerResult.commitChanges()

    #End of nodes insertion; update temp layer extents

    layerResult.updateExtents()
    
    QgsProject.instance().addMapLayer(layerResult)
    
    del url, layerResult, layerResultProvider, listFieldsNames, listNewFeats
    del bbox, xMin, xMax, yMin, yMax, listX, listY
    
    try :
        del ptReq, ptReqT, params, formatted_url, response, data, currentResult, newFeat, newGeom
    except :
        pass
    
    try :
        del code_insee, code_zone, libelle_commune, zone_sismicite
    except :
        pass



    
    

