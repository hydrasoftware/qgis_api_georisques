# -*- coding: utf-8 -*-

from qgis.core import QgsProcessingProvider
from .AlgoAPIGeorisques import AlgoAPIGeorisques
from .AlgoNomenclatureAPIGeorisques import AlgoNomenclatureAPIGeorisques
from .AlgoRapportRisquesAPIGeorisques import AlgoRapportRisquesAPIGeorisques
from .AlgoRGAAPIGeorisques import AlgoRGAAPIGeorisques

class ProcAPIGeorisquesProvider(QgsProcessingProvider):

    def __init__(self):

        QgsProcessingProvider.__init__(self)

    def unload(self):

        pass

    def loadAlgorithms(self):

        self.addAlgorithm(AlgoAPIGeorisques())
        self.addAlgorithm(AlgoNomenclatureAPIGeorisques())
        self.addAlgorithm(AlgoRapportRisquesAPIGeorisques())
        self.addAlgorithm(AlgoRGAAPIGeorisques())

    def id(self):

        return 'HYDRATEC_APIGeorisques'

    def name(self):

        return 'HYDRATEC_APIGeorisques'

    def icon(self):

        return QgsProcessingProvider.icon(self)

    def longName(self):

        return self.name()
