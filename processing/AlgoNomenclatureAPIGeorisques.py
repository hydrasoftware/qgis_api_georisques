# -*- coding: utf-8 -*-

from qgis.PyQt.QtCore import QCoreApplication, QVariant
from qgis.core import (QgsProcessing,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterVectorLayer,
                       QgsProcessingParameterEnum,
                       QgsProject,
                       QgsCoordinateReferenceSystem,
                       QgsCoordinateTransformContext,
                       QgsCoordinateTransform,
                       QgsMessageLog,
                       QgsDistanceArea,
                       Qgis,
                       QgsField,
                       QgsVectorLayer,
                       QgsFeature,
                       QgsGeometry,
                       QgsPointXY)
import requests
import datetime
import math
import json


optionsList = ['Etats documents PPR','Familles risque PPR']

class AlgoNomenclatureAPIGeorisques(QgsProcessingAlgorithm):


    OUTPUT = 'OUTPUT'
    INPUT = 'INPUT'

    def initAlgorithm(self, config):

        self.addParameter(QgsProcessingParameterEnum('options', 'Options', options=optionsList, allowMultiple=True, defaultValue=[0]))

    def processAlgorithm(self, parameters, context, feedback):
        
        date1 = datetime.datetime.now()
        
        # API url
        
        if optionsList.index('Etats documents PPR') in parameters["options"] :
        
            url = "https://www.georisques.gouv.fr/api/v1/ppr/etats_documents"
            
            #Temporary layer for resulting polygons
            layerResult = QgsVectorLayer("NoGeometry?crs=epsg:2154", "Etats documents PPR", "memory")
            layerResultProvider = layerResult.dataProvider()
            
            layerResultProvider.addAttributes([QgsField("code_etat",QVariant.String, "string", 0)])
            layerResultProvider.addAttributes([QgsField("libelle_etat",QVariant.String, "string", 0)])
            
            layerResult.updateFields()
            
            listFieldsNames = [f.name() for f in layerResult.fields()]
            
            #list for insert
            
            listNewFeats = []

            #Requests on Georisques API
            QgsMessageLog.logMessage("Request on Georisques Etats documents PPR",tag="Data Processing", level=Qgis.Info)
                    
            response = requests.get(url=url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    code_etat = currentResult["code_etat"]
                    libelle_etat = currentResult["libelle_etat"]
                        
                    newFeat = QgsFeature()
                    newFeat.setFields(layerResult.fields())
                    
                    newFeat.setAttribute("code_etat",code_etat)
                    newFeat.setAttribute("libelle_etat",libelle_etat)
                    
                    listNewFeats.append(newFeat)
                            
                            # QgsMessageLog.logMessage("listNewFeats " + str(len(listNewFeats)),tag="Data Processing", level=Qgis.Info)
                            
            QgsMessageLog.logMessage("End of iterations",tag="Data Processing", level=Qgis.Info)
            QgsMessageLog.logMessage("Remove duplicates",tag="Data Processing", level=Qgis.Info)
            
            
            layerResult.dataProvider().addFeatures(listNewFeats)
            layerResult.commitChanges()
            
            del listNewFeats
            
            QgsProject.instance().addMapLayer(layerResult)
        
        if optionsList.index('Familles risque PPR') in parameters["options"] :
        
            # API url
            
            url = "https://www.georisques.gouv.fr/api/v1/ppr/famille_risques"
            
            #Temporary layer for resulting polygons
            layerResult = QgsVectorLayer("NoGeometry?crs=epsg:2154", "Familles risque PPR", "memory")
            layerResultProvider = layerResult.dataProvider()
            
            layerResultProvider.addAttributes([QgsField("code_risque",QVariant.String, "string", 0)])
            layerResultProvider.addAttributes([QgsField("libelle_risque",QVariant.String, "string", 0)])
            layerResultProvider.addAttributes([QgsField("code_alea",QVariant.String, "string", 0)])
            layerResultProvider.addAttributes([QgsField("libelle_alea",QVariant.String, "string", 0)])
            
            layerResult.updateFields()
            
            listFieldsNames = [f.name() for f in layerResult.fields()]
            
            #list for insert
            
            listNewFeats = []
    
            #Requests on Georisques API
            QgsMessageLog.logMessage("Request on Georisques - Familles risque PPR",tag="Data Processing", level=Qgis.Info)
                    
            response = requests.get(url=url)
                
            #Jsonify API response        
            data = response.json()
            
            if "data" in data.keys() :
                
                for i in range(len(data["data"])) :
                    
                    currentResult = data["data"][i]
                                            
                    code_risque = currentResult["code_risque"]
                    libelle_risque = currentResult["libelle_risque"]
                    
                    dict_classes_hazard = currentResult["classes_alea"]
                    
                    for hazard in dict_classes_hazard :
                        
                        code = hazard["code"]
                        libelle = hazard["libelle"]
                        
                        newFeat = QgsFeature()
                        newFeat.setFields(layerResult.fields())
                        
                        newFeat.setAttribute("code_risque",code_risque)
                        newFeat.setAttribute("libelle_risque",libelle_risque)
                        newFeat.setAttribute("code_alea",code)
                        newFeat.setAttribute("libelle_alea",libelle)
                        
                        listNewFeats.append(newFeat)
            
            
            layerResult.dataProvider().addFeatures(listNewFeats)
            layerResult.commitChanges()
            
            del listNewFeats
            
            QgsProject.instance().addMapLayer(layerResult)

        return {self.OUTPUT: ''}

    def name(self):

        return 'Récupération nomenclatures - Etats documents PPR et Familles Risques PPR'

    def displayName(self):

        return self.name()

    def group(self):

        return self.groupId()

    def groupId(self):

        return "API Georisques"

    def createInstance(self):
        
        return AlgoNomenclatureAPIGeorisques()
    
    def shortHelpString(self):
         """
         Returns a localised short help string for the algorithm.
         """
         return ("L'outil permet d'interroger l'API Georisques et de convertir les résultats en table lisible sous SIG \n"  + \
                 "L'ensemble des résultats retournés par l'API sont intégrés en tant que données attributaires\n" + \
                 "<h3>Sélection des paramètres</h3>\n" + \
                 "Les paramètres sont les suivants : \n" + \
                 "- Sélection des nomenclatures à rapatrier\n " + \
                 "<h3>Exécution de l'algorithme</h3> \n" + \
                 "Pour chacune des nomenclatures à rapatrier, l'API Géorisques est requêtée.\n" + \
                 "Une couche temporaire de résultat sans géométrie est générée, avec chaque élément de nomenclature en tant qu'enregistrement\n")
