# -*- coding: utf-8 -*-

from qgis.PyQt.QtCore import QCoreApplication, QVariant, QByteArray
from qgis.core import (QgsProcessing,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterVectorLayer,
                       QgsProject,
                       QgsCoordinateReferenceSystem,
                       QgsCoordinateTransformContext,
                       QgsCoordinateTransform,
                       QgsMessageLog,
                       QgsDistanceArea,
                       Qgis,
                       QgsField,
                       QgsVectorLayer,
                       QgsFeature,
                       QgsGeometry,
                       QgsPointXY)
import requests
import datetime
import math
import json

class AlgoRGAAPIGeorisques(QgsProcessingAlgorithm):


    OUTPUT = 'OUTPUT'
    INPUT = 'INPUT'

    def initAlgorithm(self, config) :

        repProjet = QgsProject.instance().readPath("./")
        
        self.addParameter(QgsProcessingParameterVectorLayer('layerprojectpointsrequest',
                                                            "Couche de points de requête du risque de retrait-gonflement des argiles",
                                                            types=[QgsProcessing.TypeVectorPoint],
                                                            defaultValue=None))

    def processAlgorithm(self, parameters, context, feedback) :
        
        date1 = datetime.datetime.now()
        
        #General purpose parameters
        
        layers = QgsProject.instance().mapLayers().values()
        
        for layer in layers :
            
            if layer.id() == parameters['layerprojectpointsrequest'] :
            
                layerprojectpointsrequest = layer
        
        srcProjRef = QgsCoordinateReferenceSystem(layerprojectpointsrequest.crs().authid())
        proj4326 = QgsCoordinateReferenceSystem(4326)
        tform = QgsCoordinateTransform(srcProjRef, proj4326, QgsProject.instance())

        # API url
        
        url = "https://www.georisques.gouv.fr/api/v1/rga"
        
        #Temporary layer for resulting polygons
        layerResult = QgsVectorLayer("Point?crs=epsg:"+str(layerprojectpointsrequest.crs().postgisSrid()), "API Géorisques - Risque de retrait - Gonflement des argiles", "memory")
        layerResultProvider = layerResult.dataProvider()
        
        layerResultProvider.addAttributes([QgsField("codeExposition",QVariant.String, "string", 0)])
        layerResultProvider.addAttributes([QgsField("exposition",QVariant.String, "string", 0)])
        
        layerResult.updateFields()

        #list for insert
        
        listNewFeats = []
        
        QgsMessageLog.logMessage("Requete API Géorisques - Argiles gonflantes",tag="Data Processing", level=Qgis.Info)
        
        for requestPoint in layerprojectpointsrequest.getSelectedFeatures() :
            
            requestPt = requestPoint.geometry().asPoint()
            
            xReq = requestPt.x()
            yReq = requestPt.y()
                
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {

                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
            
            response = requests.get(url=formatted_url)
     
            try :
                
                data = response.json()
                                            
                codeExposition = data["codeExposition"]
                exposition = data["exposition"]
                
            except :
                
                codeExposition = 'Indéterminé'
                exposition = 'Indéterminé'
            
            newFeat = QgsFeature()
            newFeat.setFields(layerResult.fields())
            
            newGeom = ptReq
                
            newFeat.setGeometry(newGeom)
            newFeat.setAttribute("codeExposition",codeExposition)
            newFeat.setAttribute("exposition",exposition)
            
            listNewFeats.append(newFeat)

        layerResult.dataProvider().addFeatures(listNewFeats)
        layerResult.commitChanges()
        
        del listNewFeats

        #End of nodes insertion; update temp layer extents

        layerResult.updateExtents()
        
        QgsProject.instance().addMapLayer(layerResult)
        
        date2 = datetime.datetime.now()
        
        QgsMessageLog.logMessage("fin " + str(date2),tag="Data Processing", level=Qgis.Info)
        QgsMessageLog.logMessage("duree " + str(date2-date1),tag="Data Processing", level=Qgis.Info)

        return {self.OUTPUT: ''}

    def name(self):

        return 'Interrogation API Georisques - Risque retrait-gonflement des argiles en des points'

    def displayName(self):

        return self.name()

    def group(self):

        return self.groupId()

    def groupId(self):

        return "API Georisques"

    def createInstance(self):
        
        return AlgoRGAAPIGeorisques()
    
    def shortHelpString(self):
        """
        Returns a localised short help string for the algorithm.
        """
        return ("L'outil permet d'interroger l'API Georisques et de convertir les résultats en entités géographiques lisibles sous SIG \n"  + \
        "L'ensemble des résultats retournés par l'API sont intégrés en tant que données attributaires\n" + \
        "<h3>Sélection des paramètres</h3>\n" + \
        "Les paramètres sont les suivants : \n" + \
        "- couche de type Point, existante ou créée en mamoire pour une utilisation had'oc, représentant les localisations pour lesquelles le risque de retrait-gonflement des argiles est à interroger\n " + \
        "<h3>Exécution de l'algorithme</h3> \n" + \
        "<b>Une sélection active des points est obligatoire, sinon le processus ne retournera aucun résultat<b>\n" + \
        "Pour chaque point, l'API Géorisques est requêtée pour obtenir la qualification du rique de retrait-gonflement des argiles\n" + \
        "Une couche temporaire de résultat est générée, avec la qualification du risque en tant que donnée attributaire\n"
        )
