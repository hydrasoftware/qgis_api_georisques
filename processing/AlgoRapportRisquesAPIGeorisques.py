# -*- coding: utf-8 -*-

from qgis.PyQt.QtCore import QCoreApplication, QVariant, QByteArray
from qgis.core import (QgsProcessing,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterVectorLayer,
                       QgsProject,
                       QgsCoordinateReferenceSystem,
                       QgsCoordinateTransformContext,
                       QgsCoordinateTransform,
                       QgsMessageLog,
                       QgsDistanceArea,
                       Qgis,
                       QgsField,
                       QgsVectorLayer,
                       QgsFeature,
                       QgsGeometry,
                       QgsPointXY)
import requests
import datetime
import math
import json

class AlgoRapportRisquesAPIGeorisques(QgsProcessingAlgorithm):


    OUTPUT = 'OUTPUT'
    INPUT = 'INPUT'

    def initAlgorithm(self, config) :

        repProjet = QgsProject.instance().readPath("./")
        
        self.addParameter(QgsProcessingParameterVectorLayer('layerprojectpointsreports', "Couche de points d'interrogation de rapports de risques", types=[QgsProcessing.TypeVectorPoint], defaultValue=None))

    def processAlgorithm(self, parameters, context, feedback) :
        
        date1 = datetime.datetime.now()
        
        #General purpose parameters
        
        layers = QgsProject.instance().mapLayers().values()
        
        for layer in layers :
            
            if layer.id() == parameters['layerprojectpointsreports'] :
            
                layerprojectpointsreports = layer
        
        #Current projRef
        
        srcProjRef = QgsCoordinateReferenceSystem(layerprojectpointsreports.crs().authid())
        proj4326 = QgsCoordinateReferenceSystem(4326)
        tform = QgsCoordinateTransform(srcProjRef, proj4326, QgsProject.instance())
        
        # API url
        
        url = "https://www.georisques.gouv.fr/api/v1/rapport_pdf"
        
        #Temporary layer for resulting polygons
        layerResult = QgsVectorLayer("Point?crs=epsg:"+str(layerprojectpointsreports.crs().postgisSrid()), "API Géorisques - Rapports de risques", "memory")
        layerResultProvider = layerResult.dataProvider()
        
        layerResultProvider.addAttributes([QgsField("rapport_risques",QVariant.ByteArray, "binary", 0)])
        layerResultProvider.addAttributes([QgsField("format",QVariant.String, "string", 0)])
        
        layerResult.updateFields()

        #list for insert
        
        listNewFeats = []
        
        for requestPoint in layerprojectpointsreports.getSelectedFeatures() :
            
            requestPt = requestPoint.geometry().asPoint()
            
            xReq = requestPt.x()
            yReq = requestPt.y()
                
            ptReq = QgsGeometry.fromPointXY(QgsPointXY(xReq, yReq))
            ptReqT = tform.transform(ptReq.asPoint())

            params = {

                'latlon' : str(ptReqT.x()) + "," + str(ptReqT.y())
            }
            
            formatted_url = url + '?' + '&'.join(["{}={}".format(k,v) for k,v in params.items()])
                
            response = requests.get(url=formatted_url,stream=True)

            byteData = QByteArray(response.content)
                            
            newFeat = QgsFeature()
            newFeat.setFields(layerResult.fields())

            newGeom = requestPoint.geometry()
            
            newFeat.setGeometry(newGeom)
            newFeat.setAttribute("rapport_risques",byteData)
            newFeat.setAttribute("format","application/pdf")
                            
            listNewFeats.append(newFeat)

        layerResult.dataProvider().addFeatures(listNewFeats)
        layerResult.commitChanges()
        
        del listNewFeats

        #End of nodes insertion; update temp layer extents

        layerResult.updateExtents()
        
        QgsProject.instance().addMapLayer(layerResult)
        
        date2 = datetime.datetime.now()

        return {self.OUTPUT: ''}

    def name(self):

        return 'Interrogation API Georisques - Rapports de risques en des points'

    def displayName(self):

        return self.name()

    def group(self):

        return self.groupId()

    def groupId(self):

        return "API Georisques"

    def createInstance(self):
        
        return AlgoRapportRisquesAPIGeorisques()
    
    def shortHelpString(self):
        """
        Returns a localised short help string for the algorithm.
        """
        return ("L'outil permet d'interroger l'API Georisques et de convertir les résultats en entités géographiques lisibles sous SIG \n"  + \
        "L'ensemble des résultats retournés par l'API sont intégrés en tant que données attributaires\n" + \
        "<h3>Sélection des paramètres</h3>\n" + \
        "Les paramètres sont les suivants : \n" + \
        "- couche de type Point, existante ou créée en mamoire pour une utilisation had'oc, représentant les localisations pour lesquelles un rapport de risques est à générer\n " + \
        "<h3>Exécution de l'algorithme</h3> \n" + \
        "<b>Une sélection active des points est obligatoire, sinon le processus ne retournera aucun résultat<b>\n" + \
        "Pour chaque point, l'API Géorisques est requêtée pour générer un rapport de risque au format pdf\n" + \
        "Une couche temporaire est générée, avec les rapports enregistrés en tant que fichier binaire\n" + \
        "L'ouverture de la table attributaire permet d'enregistrer le fichier binaire sous un répertoire de destination.\n" + \
        "L'utilisateur devra mentionner un nom de fichier avec l'extension .pdf afin que le fichhier soit lisible.\n"
        )
