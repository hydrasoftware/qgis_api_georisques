**Plugin API Georisques**

This plugin is a project to make requests on publix French API Georisques (from french opendata initiative) by :

- get users wishes via pyqgis processing api interface
- requests informations on API Georisques
- transform data in a geospatial form (shapes are request on other info ressources if needed)
- integrate in QGIS current project

** Installation **

Download current folder or zip, and :
- Use the QGIS Plugin Manager to install via zip
- directly add the folder in the QGIS plugin folder

** Known issues **

- Requesting on french big city supporting urban districts (ie Paris, Lyon, Marseille), can lead to globalisation of results
(results for disricts can be globalized for whole city)

