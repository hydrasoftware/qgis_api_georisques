# -*- coding: utf-8 -*-

def classFactory(iface):

    from .ProcAPIGeorisquesPlugin import ProcAPIGeorisquesPlugin
    return ProcAPIGeorisquesPlugin()
